from sqlalchemy import MetaData, Table, Column, types
from migrate import *

def upgrade():
    # Upgrade operations go here. Don't create your own engine; use the engine
    # named 'migrate_engine' imported from migrate.
    meta = MetaData(bind=migrate_engine)
    t_salutation = Table(
        'salutation', meta,
        Column('id', types.Integer, primary_key=True),
        Column('name', types.Unicode(30), nullable=False, unique=True))
    t_salutation.create()

def downgrade():
    # Operations to reverse the above upgrade go here.
    meta = MetaData(bind=migrate_engine)
    t_salutation = Table('salutation', meta, autoload=True)
    t_salutation.drop()
