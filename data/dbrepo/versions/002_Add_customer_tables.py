from sqlalchemy import MetaData, Table, Column, ForeignKey, types
from migrate import *

def upgrade():
    # Upgrade operations go here. Don't create your own engine; use the engine
    # named 'migrate_engine' imported from migrate.
    meta = MetaData(bind=migrate_engine)
    t_person = Table(
        'person', meta,
        Column('id', types.Integer, primary_key=True),
        Column('firstname', types.Unicode(100), nullable=False),
        Column('lastname', types.Unicode(100), nullable=False))
    t_person.create()
    t_customer = Table(
        'customer', meta,
        Column('id', types.Integer, primary_key=True),
        Column('person_id', types.Integer, ForeignKey(t_person.c.id),
               nullable=False, unique=True))
    t_customer.create()

def downgrade():
    # Operations to reverse the above upgrade go here.
    meta = MetaData(bind=migrate_engine)
    t_customer = Table('customer', meta, autoload=True)
    t_customer.drop()
    t_person = Table('person', meta, autoload=True)
    t_person.drop()
