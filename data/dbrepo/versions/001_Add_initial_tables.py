from sqlalchemy import MetaData, Table, Column, ForeignKey, types
from migrate import *

def upgrade():
    # Upgrade operations go here. Don't create your own engine; use the engine
    # named 'migrate_engine' imported from migrate.
    meta = MetaData(bind=migrate_engine)
    t_product_type = Table(
        'producttype', meta,
        Column('id', types.Integer, primary_key=True),
        Column('name', types.Unicode(40), nullable=False),
        Column('description', types.UnicodeText(), nullable=False))
    t_product_type.create()
    t_product = Table(
        'product', meta,
        Column('id', types.Integer, primary_key=True),
        Column('name', types.Unicode(100), nullable=False),
        Column('description', types.UnicodeText(), nullable=False),
        Column('producttype_id', types.Integer,
               ForeignKey(t_product_type.c.id), nullable=False))
    t_product.create()

def downgrade():
    # Operations to reverse the above upgrade go here.
    meta = MetaData(bind=migrate_engine)
    t_product = Table('product', meta, autoload=True)
    t_product.drop()
    t_product_type = Table('product_type', meta, autoload=True)
    t_product_type.drop()
