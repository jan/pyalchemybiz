try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup(
    name='pyalchemybiz',
    version='0.1',
    description='python based small business suite.',
    author='Jan Dittberner',
    author_email='jan@dittberner.info',
    url='http://www.dittberner.info/projects/pyalchemybiz',
    install_requires=[
        "Pylons>=0.9.7",
        "SQLAlchemy>=0.5.2,<0.6",
        "sqlalchemy-migrate>=0.5.2,<0.6",
        "Mako",
        "FormBuild>=2.0,<3",
    ],
    setup_requires=[
        "PasteScript>=1.6.3",
        "Babel>=0.9.1",
    ],
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    test_suite='nose.collector',
    package_data={'pyalchemybiz': ['i18n/*/LC_MESSAGES/*.mo']},
    message_extractors = {'pyalchemybiz': [
            ('**.py', 'python', None),
            ('templates/**.mako', 'mako', {'input_encoding': 'utf-8'}),
            ('public/**', 'ignore', None)]},
    zip_safe=False,
    paster_plugins=['PasteScript', 'Pylons'],
    entry_points="""
    [paste.app_factory]
    main = pyalchemybiz.config.middleware:make_app

    [paste.app_install]
    main = pylons.util:PylonsInstaller
    """,
)
