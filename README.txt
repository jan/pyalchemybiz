This file is for you to describe the pyalchemybiz application. Typically
you would include information such as the information below:

Installation and Setup
======================

Install ``pyalchemybiz`` using easy_install::

    easy_install pyalchemybiz

Make a config file as follows::

    paster make-config pyalchemybiz config.ini

Tweak the config file as appropriate and then setup the application::

    paster setup-app config.ini

Then you are ready to go.
