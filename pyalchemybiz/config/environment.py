"""Pylons environment configuration"""
import os

from mako.lookup import TemplateLookup
from pylons import config
from pylons.error import handle_mako_error
from sqlalchemy import engine_from_config
from sqlalchemy.exc import NoSuchTableError
from migrate.versioning.api import db_version

from pyalchemybiz.model import init_model

import pyalchemybiz.lib.app_globals as app_globals
import pyalchemybiz.lib.helpers
from pyalchemybiz.config.routing import make_map

from pyalchemybiz.model import init_model

def load_environment(global_conf, app_conf):
    """Configure the Pylons environment via the ``pylons.config``
    object
    """
    # Pylons paths
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    paths = dict(root=root,
                 controllers=os.path.join(root, 'controllers'),
                 static_files=os.path.join(root, 'public'),
                 templates=[os.path.join(root, 'templates')])

    # Initialize config with the basic options
    config.init_app(global_conf, app_conf, package='pyalchemybiz', paths=paths)

    config['routes.map'] = make_map()
    config['pylons.app_globals'] = app_globals.Globals()
    config['pylons.h'] = pyalchemybiz.lib.helpers

    # Create the Mako TemplateLookup, with the default auto-escaping
    config['pylons.app_globals'].mako_lookup = TemplateLookup(
        directories=paths['templates'],
        error_handler=handle_mako_error,
        module_directory=os.path.join(app_conf['cache_dir'], 'templates'),
        input_encoding='utf-8', default_filters=['escape'],
        imports=['from webhelpers.html import escape'])

    # Setup the SQLAlchemy database engine
    engine = engine_from_config(config, 'sqlalchemy.')

    try:
        init_model(engine)
    except NoSuchTableError:
        # special handling for calls in websetup.py
        import inspect
        frame = inspect.currentframe()
        try:
            functions = [current[3] for current in \
                             inspect.getouterframes(frame)]
            if functions[1] != 'setup_app':
                raise
        finally:
            del frame

    # CONFIGURATION OPTIONS HERE (note: all config options will override
    # any Pylons config options)
