"""Setup the pyalchemybiz application"""
import logging

from paste.deploy import appconfig
from pylons import config

from sqlalchemy.exceptions import NoSuchTableError
import sys
from migrate.versioning.api import db_version, version_control, upgrade

from pyalchemybiz.config.environment import load_environment

log = logging.getLogger(__name__)


def setup_app(command, conf, vars):
    """Place any commands to setup pyalchemybiz here"""
    load_environment(conf.global_conf, conf.local_conf)

    repoversion = int(config.get('migrate.repo.version'))
    repodir = config.get('migrate.repo.dir')
    dburl = config.get('sqlalchemy.url')

    # Populate the DB on 'paster setup-app'

    log.info("Setting up database connectivity...")
    log.info("Desired database repository version: %d" % repoversion)
    log.info("Desired database repository directory: %s" % repodir)

    try:
        dbversion = int(db_version(dburl, repodir))
    except NoSuchTableError:
        version_control(dburl, repodir)
        dbversion = int(db_version(dburl, repodir))
    except Exception, e:
        log.error(e)
        raise e
    log.info("detected db version %d" % dbversion)
    if dbversion < repoversion:
        upgrade(dburl, repodir, repoversion)
    elif dbversion > repoversion:
        log.error("The database at %s is already versioned and its version " +
                  "%d is greater than the required version %d",
                  dburl, dbversion, repoversion)
        sys.exit(1)

    log.info("Successfully set up.")
