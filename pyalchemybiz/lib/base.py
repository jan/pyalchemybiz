"""The base Controller API

Provides the BaseController class for subclassing.
"""
from pylons import c, cache, config, g, request, response, session
from pylons.controllers import WSGIController
from pylons.controllers.util import abort, etag_cache, redirect_to
from pylons.decorators import jsonify, validate
from pylons.i18n.translation import _, ugettext, add_fallback
from pylons.templating import render_mako as render

import pyalchemybiz.lib.helpers as h

from pyalchemybiz.model import meta



class BaseController(WSGIController):

    def __before__(self):
        # set language environment
        for lang in request.languages:
            try:
                add_fallback(lang)
            except:
                pass

    def __call__(self, environ, start_response):
        """Invoke the Controller"""
        # WSGIController.__call__ dispatches to the Controller method
        # the request is routed to. This routing information is
        # available in environ['pylons.routes_dict']
        # connect to database
        try:
            return WSGIController.__call__(self, environ, start_response)
        finally:
            meta.Session.remove()
