<%inherit file="/base/base.mako" />

<ul id="menu">
% for item in c.menuitems:
 <li>${h.link_to(item[0], h.url_for(controller=item[1], action=item[2]))}</li>
% endfor
</ul>