<%inherit file="/base/customer.mako" />

<%def name="heading()"><h1>${_('View customer')}</h1></%def>

<table>
<tbody>
<tr>
<th>${_('First name:')}</th><td>${c.customer.person.firstname}</td>
</tr>
<tr>
<th>${_('Last name:')}</th><td>${c.customer.person.lastname}</td>
</tr>
</tbody>
</table>

<%def name="footer()">
<p>
  ${h.link_to(_('All Customers'), h.url_for(controller='customer', action='list'))}
| ${h.link_to(_('New Customer'), h.url_for(controller='customer', action='new'))}
| ${h.link_to(_('Edit Customer'), h.url_for(controller='customer', action='edit', id=c.id))}
| ${h.link_to(_('Delete Customer'), h.url_for(controller='customer', action='delete', id=c.id))}
</p>
${parent.footer()}
</%def>
