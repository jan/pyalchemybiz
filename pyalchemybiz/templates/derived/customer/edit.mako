<%inherit file="/base/customer.mako" />
<%namespace file="fields.mako" name="fields" import="*"/>
${h.form(h.url_for(controller='customer', action='save', id=c.id))}
<fieldset id="editcustomerform">
<legend>${_('Edit customer')}</legend>
${fields.body()}
${h.submit('save', _('Save changes'))}
</fieldset>
${h.end_form()}

<%def name="heading()"><h1>${_('Edit customer')}</h1></%def>
