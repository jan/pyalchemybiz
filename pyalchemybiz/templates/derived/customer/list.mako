<%inherit file="/base/customer.mako" />

<%def name="heading()"><h1>${_('Customer List')}</h1></%def>

<%def name="buildrow(customer, odd=True)">
  %if odd:
      <tr class="odd">
  %else:
      <tr class="even">
  %endif
        <td>
          ${h.link_to(
              customer.id,
              h.url_for(
                  controller=u'customer',
                  action='view',
                  id=unicode(customer.id)
              )
          )}
        </td>
        <td>
          ${customer}
        </td>
      </tr>
</%def>

%if len(c.paginator):
<p>${c.paginator.pager(_('$link_first $link_previous $first_item to $last_item of $item_count $link_next $link_last'))}</p>
<table class="paginator">
  <tr>
    <th>${_('Customer ID')}</th>
    <th>${_('Customer')}</th>
  </tr>
<% counter=0 %>
%for item in c.paginator:
    ${buildrow(item, counter%2)}
    <% counter += 1 %>
%endfor
</table>
<p>${c.paginator.pager('~2~')}</p>
%else:
<p>
 ${_('No customers have yet been created.')}
 ${h.link_to(_('Add one'), h.url_for(controller='customer', action='new'))}.
</p>
%endif
