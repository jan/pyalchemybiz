<%inherit file="/base/customer.mako" />
<%namespace file="fields.mako" name="fields" import="*"/>
${h.form(h.url_for(controller='customer', action='create'))}
<fieldset id="createcustomerform">
<legend>${_('Create new customer')}</legend>
${fields.body()}
${h.submit('submit', _('Add new customer'))}
</fieldset>
${h.end_form()}

<%def name="heading()"><h1>${_('Create new customer')}</h1></%def>
