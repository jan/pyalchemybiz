## -*- coding: utf-8 -*-
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${self.title()}</title>
    ${h.stylesheet_link('/pyalchemybiz.css')}
    ${h.javascript_link('/js/jquery.js', '/js/pyalchemybiz.js')}
    ${self.head()}
  </head>
  <body>
    <div id="header">${self.header()}</div>
    ${self.tabs()}
    ${self.menu()}
    ${self.heading()}
    ${self.breadcrumbs()}
    ${next.body()}
    ${self.footer()}
  </body>
</html>

<%def name="title()">${_('PyAlchemyBiz')}</%def>
<%def name="head()"></%def>
<%def name="header()"><a id="top"></a></%def>
<%def name="tabs()"></%def>
<%def name="menu()"></%def>
<%def name="heading()"><h1>${c.heading or _('PyAlchemyBiz')}</h1></%def>
<%def name="breadcrumbs()"></%def>
<%def name="footer()"><p><a href="#top">${_('Top')} ^</a></p></%def>
