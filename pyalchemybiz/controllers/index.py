import logging

from pyalchemybiz.lib.base import *
from pylons.i18n.translation import ugettext, _, set_lang, get_lang


log = logging.getLogger(__name__)


class IndexController(BaseController):

    def index(self):
        c.menuitems = [(_("Customer management"), "customer", "list")]
        return render('/derived/index/index.mako')
