# -*- coding: utf-8 -*-
import logging

from pyalchemybiz.lib.base import BaseController, _, c, render, redirect_to, \
    request
from pyalchemybiz.model import customer, meta, person
from pylons.decorators import validate
from pylons.decorators.rest import restrict
import formencode
from formencode import htmlfill
import webhelpers.paginate as paginate

log = logging.getLogger(__name__)


class NewCustomerForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    firstname = formencode.validators.String(
        not_empty = True,
        messages = {
            'empty': _('''Please enter customer's firstname.''')
        })
    lastname = formencode.validators.String(
        not_empty = True,
        messages = {
            'empty': _('''Please enter customer's lastname.''')
        })


class CustomerController(BaseController):
    """
    Controller for customer related operations.
    """

    def list(self):
        """
        Show a customer list.
        """
        records = meta.Session.query(customer.Customer)
        c.paginator = paginate.Page(
            records,
            page = int(request.params.get('page', 1)),
            items_per_page = 10,
        )
        return render('/derived/customer/list.mako')

    def view(self, id=None):
        """
        Display a customer's details.
        """
        if id is None:
            abort(404)
        cust_q = meta.Session.query(customer.Customer)
        c.customer = cust_q.get(int(id))
        if c.customer is None:
            abort(404)
        return render('/derived/customer/view.mako')

    def new(self):
        """
        Displays a form for creating a new customer.
        """
        return render('/derived/customer/new.mako')

    def edit(self, id=None):
        """
        Displays a form for editing customer with id.
        """
        if id is None:
            abort(404)
        cust_q = meta.Session.query(customer.Customer)
        cust = cust_q.get(int(id))
        if cust is None:
            abort(404)
        values = {
            'firstname': cust.person.firstname,
            'lastname': cust.person.lastname,
            }
        return htmlfill.render(render('/derived/customer/edit.mako'), values)

    def delete(self, id=None):
        """
        Deletes a customer.
        """
        if id is None:
            abort(404)
        cust_q = meta.Session.query(customer.Customer)
        cust = cust_q.get(int(id))
        if cust is None or cust.person is None:
            abort(404)
        meta.Session.delete(cust.person)
        meta.Session.delete(cust)
        meta.Session.commit()

        redirect_to(controller='customer', action='list')

    @restrict('POST')
    @validate(schema=NewCustomerForm(), form='new')
    def create(self):
        """
        Saves the information submitted from new() and redirects to
        list().
        """
        cust = customer.Customer()
        meta.Session.add(cust)
        cust.person = person.Person()
        for k, v in self.form_result.items():
            setattr(cust.person, k, v)
        meta.Session.add(cust.person)
        meta.Session.commit()

        redirect_to(controller='customer', action='list')

    @restrict('POST')
    @validate(schema=NewCustomerForm(), form='edit')
    def save(self, id=None):
        """
        Saves the information submitted from edit() and redirects to
        list().
        """
        cust_q = meta.Session.query(customer.Customer)
        cust = cust_q.get(int(id))
        if cust is None:
            abort(404)
        for k, v in self.form_result.items():
            if getattr(cust.person, k) != v:
                setattr(cust.person, k, v)
        meta.Session.add(cust.person)
        meta.Session.commit()

        redirect_to(controller='customer', action='list')
