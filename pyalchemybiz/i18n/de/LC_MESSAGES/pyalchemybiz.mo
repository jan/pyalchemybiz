��          �               �  Y   �     �     �                "     +     7     E     Y     i     w     �  
   �     �  #   �  "   �  !   �               ,     0  �  >  [   �     -     F     X     d     y  	        �     �     �     �     �     �  	   �     �  '   �  ,   !  -   N     |     �     �     �   $link_first $link_previous $first_item to $last_item of $item_count $link_next $link_last Add new customer Add one All Customers Create new customer Customer Customer ID Customer List Customer management Delete Customer Edit Customer Edit customer First name: Last name: New Customer No customers have yet been created. Please enter customer's firstname. Please enter customer's lastname. PyAlchemyBiz Save changes Top View customer Project-Id-Version: pyalchemybiz 0.1
Report-Msgid-Bugs-To: jan@dittberner.info
POT-Creation-Date: 2008-10-06 00:50+0200
PO-Revision-Date: 2009-03-14 22:08+0100
Last-Translator: Jan Dittberner <jan@dittberner.info>
Language-Team: de <de@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.4
 $link_first $link_previous $first_item bis $last_item von $item_count $link_next $link_last Neuen Kunden hinzufügen Einen hinzufügen Alle Kunden Neuen Kunden anlegen Kunde Kunden ID Kundenliste Kundenverwaltung Kunden löschen Kunden bearbeiten Kunden bearbeiten Vorname: Nachname: Neuer Kunde Bis jetzt wurden keine Kunden angelegt. Bitte geben Sie den Vornamen des Kunden ein. Bitte geben Sie den Nachnamen des Kunden ein. PyAlchemyBiz Änderungen speichern Zum Seitenanfang Kunden anzeigen 