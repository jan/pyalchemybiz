"""Data model for pyalchemybiz."""
import logging
import sqlalchemy as sa
from sqlalchemy import orm

from pyalchemybiz.model import meta
from pyalchemybiz.model import person, customer, product

log = logging.getLogger(__name__)


def init_model(engine):
    """Call me before using any of the tables or classes in the model."""

    sm = orm.sessionmaker(autoflush=True, autocommit=False, bind=engine)

    meta.engine = engine
    meta.Session = orm.scoped_session(sm)

    person.t_person = sa.Table(
        'person', meta.metadata, autoload=True, autoload_with=engine)
    customer.t_customer = sa.Table(
        'customer', meta.metadata, autoload=True, autoload_with=engine)
    product.t_producttype = sa.Table(
        'producttype', meta.metadata, autoload=True, autoload_with=engine)
    product.t_product = sa.Table(
        'product', meta.metadata, autoload=True, autoload_with=engine)

    orm.mapper(person.Person, person.t_person)
    orm.mapper(customer.Customer, customer.t_customer, properties={
            'person': orm.relation(person.Person, backref='customer')})
    orm.mapper(product.ProductType, product.t_producttype)
    orm.mapper(product.Product, product.t_product, properties={
            'producttype': orm.relation(product.ProductType,
                                        backref='products')})
