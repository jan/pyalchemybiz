"""Customer related model elements."""
t_customer = None


class Customer(object):
    """Customer model class."""

    def __repr__(self):
        if self.id is None:
            return "<Customer: new %s>" % (self.person)
        else:
            return "<Customer: %d %s>" % (self.id, self.person)
