t_person = None


class Person(object):

    def __repr__(self):
        if self.id is None:
            return "<Person: new %s %s>" % (self.firstname, self.lastname)
        else:
            return "<Person: %d %s %s>" % (self.id, self.firstname,
                                           self.lastname)
