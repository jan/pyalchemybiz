t_producttype = None
t_product = None


class ProductType(object):

    def __repr__(self):
        if self.id is None:
            return "<ProductType: new %s>" % (self.name)
        else:
            return "<ProductType: %d %s>" % (self.id, self.name)


class Product(object):

    def __repr__(self):
        if self.id is None:
            return "<Product: new %s %s>" % (self.name, self.producttype)
        else:
            return "<Product: %d %s %s>" % (self.id, self.name,
                                            self.producttype)
